declare variable $baseURI external;
declare variable $dbPath external;
declare variable $project external;

declare function local:list-db-resources($projectId, $dbPath, $baseURI, $dir){
    for $d in db:dir($dbPath, $dir)
    let $subDirs := local:list-db-resources($projectId, $dbPath, $baseURI, $dir||'/'||$d)
    let $depth := count(string-to-codepoints($dir)[.=string-to-codepoints('/')])
    order by $d/text()
    return

    let $fullPath := substring-after($dir || '/' || $d/text(),'/')


        return
        <ul class="collection">
            {if(ends-with($d/text(),'.xml'))
            then
                (
                for $c in doc($dbPath || '/' || $fullPath)//*:ead
                return
                <li data-href='{$baseURI}{$projectId}/{$fullPath}/info/eadheader.html' depth="{$depth}">

                {data($c//*:titleproper)}
                </li>
                )
            else
                <li data-dir='true'>
                	{<span class="summary" depth="{$depth}">{$d/text()}</span>}
                        {local:list-db-resources($projectId, $dbPath, $baseURI, $dir||'/'||$d)}

                </li>
            }
        </ul>
};

<nav id="toc">
	{local:list-db-resources($project, $dbPath, $baseURI,'')}
</nav>
