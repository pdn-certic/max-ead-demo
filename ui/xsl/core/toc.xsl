<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"                xmlns:max="https://max.unicaen.fr"
exclude-result-prefixes="max"
>

    <xsl:import href="../../../../../ui/xsl/core/i18n.xsl"/>

    <xsl:param name="project"/>
    <xsl:param name="locale"/>

    <xsl:template match="/">
        <div id="toc">
            <h1>
                <xsl:value-of select="max:i18n($project,'toc.title',$locale)"/>
            </h1>
            <xsl:apply-templates/>
        </div>
    </xsl:template>

    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()" />
        </xsl:copy>
    </xsl:template>

    <xsl:template match="ul[@class='collection']">
        <xsl:choose>
            <xsl:when test="li[@data-dir='true']">
                <details>
                    <xsl:apply-templates/>
                </details>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="li[@data-dir='true']">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="li[@data-href]">
        <div class="fichierXml c{@depth}">
            <!-- <xsl:variable name="href">
                <xsl:value-of select="replace(@data-href, 'xml', 'html')"/>
            </xsl:variable> -->
            <a href="{@data-href}">
                <xsl:apply-templates/>
            </a>
        </div>
    </xsl:template>

    <xsl:template match="span[@class='summary']">
        <summary class="c{@depth}">
            <xsl:variable name="i18nKey" select="'toc.'||./text()" />
            <xsl:value-of select="max:i18n($project,$i18nKey,$locale)"/>
        </summary>
    </xsl:template>

</xsl:stylesheet>
