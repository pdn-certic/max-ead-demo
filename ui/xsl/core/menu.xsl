<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:param name="baseURI"/>
    <xsl:param name="selectedTarget"/>
    <xsl:param name="projectId"/>

	<xsl:variable name="menuType" select="/menu/@type"/>

    <xsl:template match="/">
    	<xsl:choose>
    		<xsl:when test="$menuType='dropdown'">
    			<xsl:apply-templates select="/menu" mode="dropdown" />
    		</xsl:when>
    		<xsl:when test="$menuType='subbar'">
    			<xsl:apply-templates select="/menu" mode="subbar" />
    		</xsl:when>
    	</xsl:choose>
    </xsl:template>


	<xsl:template match="menu" mode="dropdown">
		<xsl:variable name="topEntry" select=".//entry[target/text()=$selectedTarget]"/>
		<!-- top menu -->
		<nav>
			<xsl:attribute name="class" select="$menuType" />
			<label for="toggle">☰</label>
			<input type="checkbox" id="toggle"/>
            <ul class="navbar-nav">
                <xsl:for-each select="/menu/entry">
                    <xsl:choose>
                        <!-- le sous-menu -->
                        <xsl:when test="count(./entry) > 0">
                            <li>
                                <xsl:attribute name="class">
                                	<xsl:choose>
                                		<xsl:when test=".//id/text()=$topEntry/id/text()">
                                			<xsl:value-of select="'active nav-item '||$menuType||'-menu-container'" />
                                		</xsl:when>
                                		<xsl:otherwise>
                                			<xsl:value-of select="'nav-item '||$menuType||'-menu-container'" />
                                		</xsl:otherwise>
                                	</xsl:choose>
                                </xsl:attribute>
                                <a class="{$menuType}-toggle">
                                    <xsl:choose>
                                        <xsl:when test="./label/child::*">
                                            <xsl:copy-of select="./label/child::*"/>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:value-of select="./label/text()"/>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </a>
                                <ul class="{$menuType}-menu {$menuType}_menu--animated">
                                    <xsl:for-each select="./entry">
										<xsl:choose>
											<!-- le sous-sous-menu -->
											<xsl:when test="count(./entry) > 0">
												<li>
													<xsl:attribute name="class">
														<xsl:choose>
															<xsl:when test=".//id/text()=$topEntry/id/text()">
																<xsl:value-of select="'active nav-item '||$menuType||'-submenu-container'" />
															</xsl:when>
															<xsl:otherwise>
																<xsl:value-of select="'nav-item '||$menuType||'-submenu-container'" />
															</xsl:otherwise>
														</xsl:choose>
													</xsl:attribute>
													<a class="{$menuType}-item {$menuType}-toggle">
														<xsl:choose>
															<xsl:when test="./label/child::*">
																<xsl:copy-of select="./label/child::*"/>
															</xsl:when>
															<xsl:otherwise>
																<xsl:value-of select="./label/text()"/>
															</xsl:otherwise>
														</xsl:choose>
													</a>
													<ul class="{$menuType}-submenu {$menuType}_submenu--animated">
														<xsl:for-each select="./entry">
															<li>
																<xsl:attribute name="class">
																	<xsl:choose>
																		<xsl:when test=".//id/text()=$topEntry/id/text()">
																			<xsl:value-of select="'active nav-item'" />
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="'nav-item'" />
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:attribute>
																<a class="{$menuType}-item" href="{$baseURI}{$projectId}/{./target/text()}">
																	<xsl:choose>
																		<xsl:when test="./label/child::*">
																			<xsl:copy-of select="./label/child::*"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="./label/text()"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</a>
															</li>
														</xsl:for-each>
													</ul>
												</li>
											</xsl:when>
											<xsl:otherwise>
												<li>
													<xsl:attribute name="class">
														<xsl:choose>
															<xsl:when test=".//id/text()=$topEntry/id/text()">
																<xsl:value-of select="'active nav-item'" />
															</xsl:when>
															<xsl:otherwise>
																<xsl:value-of select="'nav-item'" />
															</xsl:otherwise>
														</xsl:choose>
													</xsl:attribute>
													<a class="{$menuType}-item" href="{$baseURI}{$projectId}/{./target/text()}">
														<xsl:choose>
															<xsl:when test="./label/child::*">
																<xsl:copy-of select="./label/child::*"/>
															</xsl:when>
															<xsl:otherwise>
																<xsl:value-of select="./label/text()"/>
															</xsl:otherwise>
														</xsl:choose>
													</a>
												</li>
											</xsl:otherwise>
										</xsl:choose>
                                    </xsl:for-each>
                                </ul>
                            </li>
                        </xsl:when>
                        <xsl:otherwise>
                            <li>
								<xsl:attribute name="class">
									<xsl:choose>
										<xsl:when test=".//id/text()=$topEntry/id/text()">
											<xsl:value-of select="'active nav-item'" />
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="'nav-item'" />
										</xsl:otherwise>
									</xsl:choose>
								</xsl:attribute>
                                <a href="{$baseURI}{$projectId}/{./target/text()}">
                                    <xsl:choose>
                                        <xsl:when test="./label/child::*">
                                            <xsl:copy-of select="./label/child::*"/>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:value-of select="./label/text()"/>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </a>
                            </li>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:for-each>
            </ul>
        </nav>
    </xsl:template>


	<xsl:template match="menu" mode="subbar">
		<xsl:variable name="topEntry" select=".//entry[target/text()=$selectedTarget]"/>
		<!-- top menu -->
		<nav>
			<xsl:attribute name="class" select="$menuType" />
			<label for="toggle">☰</label>
			<input type="checkbox" id="toggle"/>
            <ul class="navbar-nav">
                <xsl:for-each select="/menu/entry">
                    <xsl:choose>
                        <!-- le sous-menu -->
                        <xsl:when test="count(./entry) > 0">
                            <li>
								<xsl:attribute name="class">
									<xsl:choose>
										<xsl:when test=".//id/text()=$topEntry/id/text()">
											<xsl:value-of select="'active nav-item '||$menuType||'-menu-container'" />
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="'nav-item '||$menuType||'-menu-container'" />
										</xsl:otherwise>
									</xsl:choose>
								</xsl:attribute>
                                <a class="{$menuType}-toggle">
                                	<xsl:if test="target">
                                		<xsl:attribute name="href" select="$baseURI||$projectId||'/'||./target/text()" />
                                	</xsl:if>
                                    <xsl:choose>
                                        <xsl:when test="./label/child::*">
                                            <xsl:copy-of select="./label/child::*"/>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:value-of select="./label/text()"/>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </a>
                                <ul class="{$menuType}-menu">
                                    <xsl:for-each select="./entry">
										<xsl:choose>
											<!-- le sous-sous-menu -->
											<xsl:when test="count(./entry) > 0">
												<li>
													<xsl:attribute name="class">
														<xsl:choose>
															<xsl:when test=".//id/text()=$topEntry/id/text()">
																<xsl:value-of select="'active nav-item dropdown-menu-container'" />
															</xsl:when>
															<xsl:otherwise>
																<xsl:value-of select="'nav-item dropdown-menu-container'" />
															</xsl:otherwise>
														</xsl:choose>
													</xsl:attribute>
													<a class="{$menuType}-item dropdown-toggle">
														<xsl:choose>
															<xsl:when test="./label/child::*">
																<xsl:copy-of select="./label/child::*"/>
															</xsl:when>
															<xsl:otherwise>
																<xsl:value-of select="./label/text()"/>
															</xsl:otherwise>
														</xsl:choose>
													</a>
													<ul class="dropdown-menu dropdown_menu--animated">
														<xsl:for-each select="./entry">
															<li>
																<xsl:attribute name="class">
																	<xsl:choose>
																		<xsl:when test=".//id/text()=$topEntry/id/text()">
																			<xsl:value-of select="'active nav-item'" />
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="'nav-item'" />
																		</xsl:otherwise>
																	</xsl:choose>
																</xsl:attribute>
																<a class="{$menuType}-item" href="{$baseURI}{$projectId}/{./target/text()}">
																	<xsl:choose>
																		<xsl:when test="./label/child::*">
																			<xsl:copy-of select="./label/child::*"/>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:value-of select="./label/text()"/>
																		</xsl:otherwise>
																	</xsl:choose>
																</a>
															</li>
														</xsl:for-each>
													</ul>
												</li>
											</xsl:when>
											<xsl:otherwise>
												<li>
													<xsl:attribute name="class">
														<xsl:choose>
															<xsl:when test=".//id/text()=$topEntry/id/text()">
																<xsl:value-of select="'active nav-item'" />
															</xsl:when>
															<xsl:otherwise>
																<xsl:value-of select="'nav-item'" />
															</xsl:otherwise>
														</xsl:choose>
													</xsl:attribute>
													<a class="{$menuType}-item" href="{$baseURI}{$projectId}/{./target/text()}">
														<xsl:choose>
															<xsl:when test="./label/child::*">
																<xsl:copy-of select="./label/child::*"/>
															</xsl:when>
															<xsl:otherwise>
																<xsl:value-of select="./label/text()"/>
															</xsl:otherwise>
														</xsl:choose>
													</a>
												</li>
											</xsl:otherwise>
										</xsl:choose>
                                    </xsl:for-each>
                                </ul>
                            </li>
                        </xsl:when>
                        <xsl:otherwise>
                            <li>
								<xsl:attribute name="class">
									<xsl:choose>
										<xsl:when test=".//id/text()=$topEntry/id/text()">
											<xsl:value-of select="'active nav-item'" />
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="'nav-item'" />
										</xsl:otherwise>
									</xsl:choose>
								</xsl:attribute>
                                <a href="{$baseURI}{$projectId}/{./target/text()}">
                                    <xsl:choose>
                                        <xsl:when test="./label/child::*">
                                            <xsl:copy-of select="./label/child::*"/>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:value-of select="./label/text()"/>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </a>
                            </li>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:for-each>
            </ul>
        </nav>
    </xsl:template>


</xsl:stylesheet>
