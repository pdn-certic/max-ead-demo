<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:ead="urn:isbn:1-931666-22-9"
    xmlns:max="https://max.unicaen.fr"
    version="2.0"
    exclude-result-prefixes="ead xsl xlink max">
    <xsl:output method="xhtml" encoding="utf-8"/>

    <xsl:param name="baseuri"/>
    <xsl:param name="key"/>
    <xsl:param name="project"/>
    <xsl:param name="locale"/>
    <xsl:param name="docName"/>
    <xsl:param name="imagesRepository"/>

    <xsl:template match="/">
        <xsl:apply-templates/>
    </xsl:template>

     <xsl:template match="ead:*[@audience='internal']"/>

    <!-- seul le premier c se trouve dans un div#text -->
	<xsl:template match="/ead:c">
        <div class="ead_style text" id="text">
            <xsl:apply-templates/>
        </div>
    </xsl:template>


   
  </xsl:stylesheet>
